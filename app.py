"""This is a simple API"""

import os
from http import HTTPStatus
from flask import Flask, request

APP = Flask(__name__)

@APP.route('/')
def hello():
    """Simple endpoint for /"""
    return 'Hello, App!'


@APP.route('/version')
def version():
    """Simple endpoint for /version"""
    return os.getenv('APP_VERSION', 'No version')
